package org.nrg.populate.util

import org.apache.commons.lang3.StringUtils

class XMLUtils {

    static String readXML(String filePath) {
        new File(filePath).text.replace('$url', StringUtils.stripEnd(Globals.siteUrl, '/'))
    }

    static void extractExperimentXmlFromXnat(String restPath, File destinationFile) {
        String xml = Globals.xnatClient.queryBase().queryParam('format', 'xml').get(Globals.xnatClient.formatXnatUrl(restPath)).
            then().assertThat().statusCode(200).and().extract().response().asString()

        xml = xml.replace(StringUtils.stripEnd(Globals.siteUrl, '/'), '$url')
        xml = xml.replaceAll(' ID=\".+?\"', '') // get rid of assessor's accession number
        xml = xml.replaceAll('<xnat:subject_ID>.+?</xnat:subject_ID>\\s?<', '<') // drop subject's accession number
        xml = xml.replaceAll('<xnat:imageSession_ID>.+?</xnat:imageSession_ID>\\s?<', '<') // drop session's accession number
        xml = xml.replaceAll('<xnat:out>[\\s\\S]+?</xnat:out>\\s?<', '<') // drop file refs
        xml = xml.replaceAll('<!--[\\s\\S]+?-->', '') // remove comments
        xml = xml.replaceAll('\n+', '\n')
        destinationFile << xml
    }

}