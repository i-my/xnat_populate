package org.nrg.populate.util

import org.nrg.populate.DataManager
import org.nrg.populate.enums.ActionRequired
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.xnat.pogo.Project

import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDate
import java.time.ZoneId

class PathUtils {

    static String defaultPathForYaml(String project) {
        "data/${project}_metadata.yaml"
    }

    static String defaultPathForZip(String project) {
        "data/${project}.zip"
    }

    static String defaultPathForData(String project) {
        "data/${project}"
    }

    static String[] possibleYamlPaths(String project) {
        [defaultPathForYaml(project), "sample_projects/${project}_metadata.yaml"]
    }

    static String[] possibleDataPaths(String project) {
        [defaultPathForData(project), "sample_projects/${project}"]
    }

    static String[] possibleZipPaths(String project) {
        [defaultPathForZip(project)]
    }

    static String findYaml(String project, DataManager dataManager) {
        for (String path in possibleYamlPaths(project)) {
            if (new File(path).exists()) {
                println("Metadata YAML for ${project} found in: ${path}")
                return path
            } else {
                println("Metadata YAML for ${project} not found in: ${path}...")
            }
        }
        try {
            dataManager.downloadYaml(project)
            return defaultPathForYaml(project)
        } catch (Throwable ignored) {
            ConsoleUtils.warn("WARNING: Metadata.yaml for ${project} not found.")
            return null
        }
    }

    static ActionRequired locateData(Project project) {
        final XPopProjectSetupExtension extension = project.extension as XPopProjectSetupExtension
        final String src = extension.src
        final String projectId = project.id
        final File srcFile = (src != null) ? new File(src) : null
        final LocalDate lastUpdated = extension.lastUpdated
        final String remoteSource = extension.remoteSource

        if (lastUpdated != null) {
            final long thresholdTime = lastUpdated.minusDays(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() // rewind to the beginning of the day preceding the update day to estimate the earliest valid data time. This gives plenty of leeway to handle the case where the data was downloaded some time during the update day and/or timezone traps.
            final File dataFolder = new File(defaultPathForData(projectId))
            final File dataZip = new File(defaultPathForZip(projectId))

            if (dataFolder.exists() && dataFolder.isDirectory() && dataFolder.lastModified() < thresholdTime) {
                dataFolder.deleteDir()
                ConsoleUtils.inform("Data folder is older than lastUpdated time specified for project ${projectId}. Folder will be deleted to force download of newest data.")
            }

            if (dataZip.exists() && dataZip.isFile() && dataZip.lastModified() < thresholdTime) {
                dataZip.delete()
                ConsoleUtils.inform("Data zip is older than lastUpdated time specified for project ${projectId}. Zip will be deleted to force download of newest data.")
            }
        }

        if (src == 'empty') {
            extension.setDataPath(defaultPathForData(projectId))
            return ActionRequired.NONE
        }

        if (src != null) {
            if (srcFile.exists() && srcFile.isDirectory()) {
                extension.setDataPath(src)
                println("Data folder for ${projectId} found in: ${src}")
                return ActionRequired.NONE
            }
        }

        for (String path in possibleDataPaths(project.getId())) {
            if (new File(path).exists() && new File(path).isDirectory()) {
                extension.setDataPath(path)
                println("Data folder for ${projectId} found in: ${path}")
                return ActionRequired.NONE
            }
        }

        if (src != null) {
            if (srcFile.exists() && srcFile.isFile()) {
                extension.setDataPath(src)
                println("Data zip for ${projectId} found in: ${src}")
                return ActionRequired.UNZIP
            } else {
                ConsoleUtils.warn("WARNING: project src specified, but project data could not be found at the path: ${src}")
            }
        }

        for (String path in possibleZipPaths(projectId)) {
            if (new File(path).exists() && new File(path).isFile()) {
                extension.setDataPath(path)
                println("Data zip for ${projectId} found in: ${path}")
                return ActionRequired.UNZIP
            }
        }

        ConsoleUtils.inform("Data zip for ${projectId} not found. Download from NRG download site will be attempted.")
        return ActionRequired.DOWNLOAD
    }

    static Path exportDir(String projectId) {
        Paths.get('export').resolve(projectId)
    }
}