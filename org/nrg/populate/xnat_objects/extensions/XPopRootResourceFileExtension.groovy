package org.nrg.populate.xnat_objects.extensions

import org.nrg.xnat.pogo.extensions.ResourceFileExtension
import org.nrg.xnat.pogo.resources.ResourceFile

class XPopRootResourceFileExtension extends ResourceFileExtension {

    XPopRootResourceFileExtension(ResourceFile resourceFile) {
        super(resourceFile)
    }

    @Override
    File getJavaFile() {
        new File(parentObject.name)
    }

}
