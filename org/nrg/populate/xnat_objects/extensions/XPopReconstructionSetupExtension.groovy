package org.nrg.populate.xnat_objects.extensions

import org.nrg.populate.util.Globals
import org.nrg.xnat.interfaces.XnatInterface
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Reconstruction
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.extensions.reconstruction.ReconstructionQueryPutExtension

class XPopReconstructionSetupExtension extends ReconstructionQueryPutExtension {

    XPopReconstructionSetupExtension(Reconstruction reconstruction) {
        super(Globals.xnatClient, reconstruction)
    }

    @Override
    void create(Project project, Subject subject, ImagingSession session) {
        super.create(project, subject, session)
        println("Reconstruction '${parentObject.label}' successfully created.")
    }

}
