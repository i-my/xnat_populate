package org.nrg.populate.xnat_objects.extensions

import org.nrg.testing.CommonStringUtils
import org.nrg.xnat.pogo.extensions.ResourceFileExtension
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile
import org.nrg.xnat.rest.SerializationUtils

class XPopResourceFileExtension extends ResourceFileExtension {

    XPopResourceFileExtension(ResourceFile resourceFile) {
        super(resourceFile)
    }

    @Override
    File getJavaFile() {
        new File("${(parentObject.resourceFolder.extension as ResourcePathExtension).effectivePath}/${parentObject.name}")
    }

    @Override
    void uploadTo(Resource resource) {
        xnatInterface.queryBase().queryParams(SerializationUtils.serializeToMap(parentObject)).multiPart(getJavaFile()).put(CommonStringUtils.formatUrl(xnatInterface.resourceFilesUrl(resource), parentObject.name.split('/').last())).then().assertThat().statusCode(200)
    }

}
