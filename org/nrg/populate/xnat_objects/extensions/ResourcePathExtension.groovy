package org.nrg.populate.xnat_objects.extensions

import org.apache.commons.lang3.StringUtils
import org.nrg.xnat.pogo.Extension
import org.nrg.xnat.pogo.resources.Resource

class ResourcePathExtension extends Extension<Resource> {

    String subFolder // allow resources to sit in a folder within the project folder

    ResourcePathExtension(Resource resource) {
        super(resource)
    }

    String getEffectivePath() {
        final String path = (parentObject.project.extension as XPopProjectSetupExtension).dataPath
        (StringUtils.isEmpty(subFolder)) ? path : "${path}/${subFolder}"
    }

}
