package org.nrg.populate.xnat_objects.extensions.experiments

import org.nrg.populate.util.Globals
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.SessionAssessor
import org.nrg.xnat.pogo.extensions.session_assessor.SessionAssessorXMLExtension

class DefaultSessionAssessorExtension extends SessionAssessorXMLExtension {

    DefaultSessionAssessorExtension(SessionAssessor assessor) {
        super(Globals.xnatClient, assessor, null)
    }

    @Override
    void create(Project project, Subject subject, ImagingSession session) {
        setAssessorXML(new File("${(project.extension as XPopProjectSetupExtension).dataPath}/${parentObject}.xml"))
        super.create(project, subject, session)
        println("Successfully uploaded session assessor: ${parentObject}")
    }

}
