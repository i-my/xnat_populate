package org.nrg.populate.xnat_objects.extensions.experiments.export

import com.fasterxml.jackson.databind.JsonSerializer
import org.nrg.populate.jackson.serializers.XPopExportSessionAssessorSerializer

class DefaultSessionAssessorExportExtension extends SessionAssessorExportExtension {

    @Override
    JsonSerializer serializer() {
        XPopExportSessionAssessorSerializer.INSTANCE
    }

}
