package org.nrg.populate.xnat_objects.extensions.experiments.export

import com.fasterxml.jackson.databind.JsonSerializer
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.extensions.subject_assessor.SubjectAssessorExtension

abstract class SubjectAssessorExportExtension extends SubjectAssessorExtension {

    SubjectAssessorExportExtension() {
        super(null)
    }

    @Override
    void create(Project project, Subject subject) {
        throw new UnsupportedOperationException('This family of extensions is used for exporting in XPOP.')
    }

    abstract JsonSerializer serializer()

}
