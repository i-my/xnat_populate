package org.nrg.populate.xnat_objects.extensions.experiments

import org.nrg.populate.util.Globals
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.SubjectAssessor
import org.nrg.xnat.pogo.extensions.subject_assessor.SubjectAssessorQueryPutExtension

class DefaultSubjectAssessorExtension extends SubjectAssessorQueryPutExtension {

    DefaultSubjectAssessorExtension(SubjectAssessor experiment) {
        super(Globals.xnatClient, experiment)
    }

    @Override
    void create(Project project, Subject subject) {
        super.create(project, subject)
        println("Successfully uploaded subject assessor: ${parentObject}")
    }

}
