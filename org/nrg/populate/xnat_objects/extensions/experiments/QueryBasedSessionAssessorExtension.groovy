package org.nrg.populate.xnat_objects.extensions.experiments

import org.nrg.populate.util.Globals
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.SessionAssessor
import org.nrg.xnat.pogo.extensions.session_assessor.SessionAssessorQueryPutExtension

class QueryBasedSessionAssessorExtension extends SessionAssessorQueryPutExtension {

    QueryBasedSessionAssessorExtension(SessionAssessor simpleAssessor) {
        super(Globals.xnatClient, simpleAssessor)
    }

    @Override
    void create(Project project, Subject subject, ImagingSession session) {
        super.create(project, subject, session)
        println("Created session assessor ${parentObject}.")
    }

}
