package org.nrg.populate.xnat_objects.extensions.experiments

import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.SubjectAssessor

class SecondarySubjectSessionExtension extends DefaultSessionExtension {

    SecondarySubjectSessionExtension(ImagingSession session) {
        super(session)
    }

    Map getDefaultPipelineParams(Project project, Subject subject) {
        ['project': project.id, 'subject': xnatInterface.getAccessionNumber(subject), 'xnat_id': xnatInterface.getAccessionNumber(parentObject as SubjectAssessor), 'sessionId': parentObject.label]
    }

    @Override
    void create(Project project, Subject subject) {
        super.create(project, null) // exclude subject since this causes XNAT to attempt to create duplicate subject and fail (in 1.7.3, at least)
    }

}
