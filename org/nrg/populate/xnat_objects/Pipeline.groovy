package org.nrg.populate.xnat_objects

import com.jayway.restassured.http.ContentType
import com.jayway.restassured.response.Response
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.xnat.rest.SerializationUtils

class Pipeline {

    private String name
    private Map<String, String> pipelineProperties = new HashMap<>()

    Pipeline(String name, Map<String, String> props) {
        this.name = name
        if (props != null) this.pipelineProperties.putAll(props)
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    void put(String property, String value) {
        pipelineProperties[property] = value
    }

    void putAll(Map<String, String> additionalProperties) {
        pipelineProperties.putAll(additionalProperties)
    }

    Pipeline deepCopy() {
        new Pipeline(name, pipelineProperties)
    }

    void addToSite() {
        pipelineProperties.put('eventSubmit_doAdd', 'Add')
        setup("Successfully added pipeline ${name} to the site.", "Failed to add pipeline ${name} to the site")
        pipelineProperties.remove('eventSubmit_doAdd')
    }

    void addToProject() {
        pipelineProperties.put('eventSubmit_doAddprojectpipeline', 'Submit')
        setup("Successfully added pipeline ${name} to project.", "Failed to add pipeline ${name} to project")
        pipelineProperties.remove('eventSubmit_doAddprojectpipeline')
    }

    private void setup(String successMessage, String failureMessage) {
        final Response response = Globals.xnatClient.requestWithCsrfToken().queryParams(SerializationUtils.serializeToMap(pipelineProperties)).contentType(ContentType.URLENC).post(Globals.xnatClient.formatXnatUrl('/app/action/ManagePipeline'))
        if (response.statusCode == 200) {
            println(successMessage)
        } else {
            ConsoleUtils.warn("${failureMessage}, status code ${response.statusCode}.")
        }
    }

}
