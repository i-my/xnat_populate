package org.nrg.populate.xnat_objects

import com.jayway.restassured.http.ContentType
import org.nrg.populate.util.ConsoleUtils
import org.nrg.populate.util.Globals
import org.nrg.populate.util.YamlUtils
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.xnat.pogo.containers.Command
import org.nrg.xnat.pogo.containers.Image
import org.nrg.xnat.pogo.containers.Wrapper
import org.nrg.xnat.pogo.events.Subscription

import static org.nrg.populate.util.Globals.xnatClient

class ContainerRegistry {

    private static final String IMAGE_DEFINITIONS = 'knownContainers.yaml'
    static final List<Image> DEFINED_IMAGES = YamlUtils.deserializeYaml(IMAGE_DEFINITIONS, Image[])

    static List<Command> knownCommands() {
        DEFINED_IMAGES.commands.flatten() as List<Command>
    }

    static List<Wrapper> knownWrappers() {
        knownCommands().wrappers.flatten() as List<Wrapper>
    }

    static void pullImages() {
        if (Globals.projects.any { (it.extension as XPopProjectSetupExtension).requiresContainerService() }) {
            Globals.projects.each { project ->
                DEFINED_IMAGES.addAll((project.extension as XPopProjectSetupExtension).imageDefinitions)
            }
            final List<Image> requiredImages = Globals.projects.collectMany { project ->
                final XPopProjectSetupExtension extension = project.extension as XPopProjectSetupExtension
                final List<Image> requiredImages = []
                extension.requiredCommandWrappers.each { wrapper ->
                    if (extension.imageDefinitions.any { image ->
                        image.commands.any { command ->
                            command.wrappers.any {
                                it.uniqueAlias == wrapper
                            }
                        }
                    }) return
                    final Image foundImage = DEFINED_IMAGES.find { image ->
                        image.commands.any { command ->
                            command.wrappers.any {
                                it.uniqueAlias == wrapper
                            }
                        }
                    }
                    if (foundImage != null) {
                        requiredImages << foundImage
                    } else {
                        throw new RuntimeException("Project ${project} required a command wrapper identified by ${wrapper}, but it could not be found in either the registry file ${IMAGE_DEFINITIONS} or the project definition.")
                    }
                }
                requiredImages + extension.imageDefinitions
            }

            List<Map> installedCommands = readKnownCommands()
            List<String> installedImages = readKnownImages()

            List<Image> problemImages, satisfiedImages
            (problemImages, satisfiedImages) = requiredImages.split { isProblematic(it, installedCommands) }
            satisfiedImages.each { image ->
                println("Required container image ${image} is already installed...")
            }
            problemImages.each { image ->
                final String imageTag = image.toString()
                if (imageTag in installedImages) {
                    println("At least one required command was missing from image ${imageTag}. Attempting to regenerate commands from image directly...")
                    xnatClient.queryBase().queryParam('image', imageTag).post(xnatClient.formatXapiUrl('/docker/images/save')).then().assertThat().statusCode(200)
                } else {
                    println("Installing required container image ${imageTag}...")
                    xnatClient.queryBase().queryParam('save-commands', true).queryParam('image', imageTag).post(xnatClient.formatXapiUrl('/docker/pull')).then().assertThat().statusCode(200)
                }
            }

            if (!problemImages.isEmpty()) {
                installedCommands = readKnownCommands()
                installedImages = readKnownImages()
                problemImages = requiredImages.findAll { isProblematic(it, installedCommands) }
                if (!problemImages.isEmpty()) {
                    println("Images ${problemImages} did not have commands defined on the images themselves, attempting to look up the commands locally...")
                    problemImages.each() { image ->
                        image.commands.each() { command ->
                            if (!commandRecognized(image, installedCommands, command)) {
                                final File potentialCommandDefinition = new File("commands/${command.name}.json")
                                if (potentialCommandDefinition.exists()) {
                                    xnatClient.queryBase().queryParam('image', image).body(potentialCommandDefinition.text).contentType(ContentType.JSON).post(xnatClient.formatXapiUrl('/commands')).then().assertThat().statusCode(201)
                                } else {
                                    throw new RuntimeException("The command ${command.name} for image ${image} was not specified in the image itself, and I can't find a local definition for it!")
                                }
                            }
                        }
                    }
                }
                installedCommands = readKnownCommands()
                installedImages = readKnownImages()
            }

            requiredImages.each { image ->
                final String imageTag = image.toString()
                image.commands.each { command ->
                    final Map commandMap = installedCommands.find { it.image == imageTag && it.name == command.name }
                    command.id(commandMap.get('id') as int)
                    command.wrappers.each { wrapper ->
                        wrapper.id(commandMap.get('xnat').find { it.name == wrapper.name }.get('id') as int)
                        println("Found command wrapper ${wrapper.name} (unique alias '${wrapper.uniqueAlias}').")
                    }
                }
            }

            Globals.projects.each { project ->
                project.subscriptions.removeAll { subscription ->
                    if (subscription.actionKey.startsWith(Subscription.COMMAND_ACTION_PROVIDER)) {
                        final String requestedCommand = subscription.actionKey.split(':')[1]
                        final Wrapper wrapper = knownWrappers().find { it.uniqueAlias == requestedCommand }
                        if (wrapper == null) {
                            ConsoleUtils.warn("I don't know of a command identified by ${requestedCommand}. The Event subscription for project ${project} that requested to use this command will be omitted.")
                            return true
                        } else {
                            subscription.setActionKey("${Subscription.COMMAND_ACTION_PROVIDER}:${wrapper.id}")
                            return false
                        }
                    } else {
                        return false
                    }
                }
            }
        }
    }

    // problematic := images that have at least one command that isn't recognized
    private static boolean isProblematic(Image image, List<Map> installedCommands) {
        image.commands.any { command ->
            !commandRecognized(image, installedCommands, command)
        }
    }

    private static boolean commandRecognized(Image image, List<Map> installedCommands, Command command) {
        installedCommands.any { it.image == image.toString() && it.name == command.name }
    }

    private static List<Map> readKnownCommands() {
        xnatClient.queryBase().get(xnatClient.formatXapiUrl('commands')).then().assertThat().statusCode(200).and().extract().response().jsonPath().getList('')
    }

    private static List<String> readKnownImages() {
        xnatClient.queryBase().get(xnatClient.formatXapiUrl('docker/images')).then().assertThat().statusCode(200).and().extract().response().jsonPath().getList('collect { it.tags }.flatten()')
    }

}
