package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatSubjectInjector
import org.nrg.populate.xnat_objects.experiments.SimpleSession
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject

class XnatSimpleSessionInjector implements XnatSubjectInjector, ArchetypalInjectorBase {

    @Override
    void injectData(Project project, Subject subject) {
        replacements.put('$subjectLabel', subject.label)
        subject.addExperiment(read(SimpleSession))
    }

}
