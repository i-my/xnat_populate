package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatProjectInjector
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject

class XnatReadFoldersAsSubjectsInjector implements XnatProjectInjector, ArchetypalInjectorBase {

    List<String> subdirectories

    @Override
    void injectData(Project project) {
        final String projectDataFolder = (project.extension as XPopProjectSetupExtension).dataPath
        subdirectories.each { subdirectory ->
            replacements.put('$subdirectory', subdirectory)
            new File("${projectDataFolder}/${subdirectory}").listFiles().each { file ->
                if (file.isDirectory()) {
                    replacements.put('$subjectFolder', file.name)
                    project.addSubject(read(Subject))
                }
            }
        }
    }

}
