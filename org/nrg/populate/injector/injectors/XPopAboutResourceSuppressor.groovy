package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatProjectInjector
import org.nrg.populate.util.Globals
import org.nrg.xnat.pogo.Project

class XPopAboutResourceSuppressor implements XnatProjectInjector {

    @Override
    void injectData(Project project) {
        project.projectResources.remove(project.projectResources.find { it.folder == Globals.XPOP_METADATA_RESOURCE })
    }

}
