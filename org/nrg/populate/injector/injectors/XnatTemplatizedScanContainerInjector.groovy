package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatScanInjector
import org.nrg.populate.xnat_objects.extensions.XPopScanSetupExtension
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan

class XnatTemplatizedScanContainerInjector implements XnatScanInjector, ArchetypalInjectorBase {

    @Override
    void injectData(Project project, Subject subject, ImagingSession session, Scan scan) {
        final XPopScanSetupExtension extension = (scan.extension ?: new XPopScanSetupExtension(scan)) as XPopScanSetupExtension
        extension.setContainers(objectMapper.readValue(archetype, CustomDeserializer.NESTED_STRING_MAP))
    }

}
