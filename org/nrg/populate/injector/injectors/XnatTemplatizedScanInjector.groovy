package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatSessionInjector
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan

class XnatTemplatizedScanInjector implements XnatSessionInjector, ArchetypalInjectorBase {

    @Override
    void injectData(Project project, Subject subject, ImagingSession session) {
        replacements.put('$sessionLabel', session.label)
        session.addScan(read(Scan))
    }

}
