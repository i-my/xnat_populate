package org.nrg.populate.injector.injectors

import org.nrg.populate.injector.XnatSessionInjector
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionExtension
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession

class XnatTemplatizedSessionContainerInjector implements XnatSessionInjector, ArchetypalInjectorBase {

    @Override
    void injectData(Project project, Subject subject, ImagingSession session) {
        final DefaultSessionExtension extension = (session.extension ?: new DefaultSessionExtension(session)) as DefaultSessionExtension
        extension.setContainers(objectMapper.readValue(archetype, CustomDeserializer.NESTED_STRING_MAP))
    }

}
