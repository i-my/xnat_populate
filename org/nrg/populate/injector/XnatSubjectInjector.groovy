package org.nrg.populate.injector

import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject

interface XnatSubjectInjector extends XnatParserInjector {

    void injectData(Project project, Subject subject)

}