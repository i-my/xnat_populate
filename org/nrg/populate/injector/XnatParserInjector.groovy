package org.nrg.populate.injector

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import org.nrg.populate.injector.injectors.*

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = 'type'
)
@JsonSubTypes([
        @JsonSubTypes.Type(value = XnatReadFoldersAsSubjectsInjector, name = 'folders_as_subjects'),
        @JsonSubTypes.Type(value = XPopDefaultUsersSuppressor, name = 'suppress_xpop_users'),
        @JsonSubTypes.Type(value = XPopAboutResourceSuppressor, name = 'suppress_xpop_resources'),
        @JsonSubTypes.Type(value = XnatSimpleSessionInjector, name = 'simple_session_inject'),
        @JsonSubTypes.Type(value = XnatTemplatizedScanContainerInjector, name = 'scan_container_inject'),
        @JsonSubTypes.Type(value = XnatTemplatizedSessionContainerInjector, name = 'session_container_inject'),
        @JsonSubTypes.Type(value = XnatTemplatizedScanInjector, name = 'scan_inject'),
        @JsonSubTypes.Type(value = XnatScanResourceInjector, name = 'scan_resource_inject')
])
trait XnatParserInjector {

    ObjectMapper objectMapper

    @JsonIgnore
    void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper
    }

}