package org.nrg.populate.jackson.object_mapping

import org.nrg.populate.jackson.modules.XPopYamlExportSerializationModule

class YamlWriter extends XPopYamlMapper {

    YamlWriter() {
        super()
        registerModule(new XPopYamlExportSerializationModule())
    }

}
