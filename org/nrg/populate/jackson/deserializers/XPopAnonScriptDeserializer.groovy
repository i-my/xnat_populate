package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.extensions.anon.AnonScriptFileExtension
import org.nrg.xnat.pogo.extensions.anon.AnonScriptFromUrlExtension

class XPopAnonScriptDeserializer extends CustomDeserializer<AnonScript> {

    @Override
    AnonScript deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final Map.Entry<String, JsonNode> entry = ++node.fields()
        final AnonScript script = new AnonScript()
        if (entry.key == 'url') {
            script.extension(new AnonScriptFromUrlExtension(script, entry.value.asText(), null))
        } else if (entry.key == 'file') {
            script.extension(new AnonScriptFileExtension(script, entry.value.asText()))
        } else {
            throw new IOException("Key for anon script must be either 'file' or 'url'.")
        }
        script
    }

}
