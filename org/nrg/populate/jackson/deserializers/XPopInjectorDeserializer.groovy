package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.injector.XnatParserInjector
import org.nrg.xnat.jackson.deserializers.CustomDeserializer

class XPopInjectorDeserializer extends CustomDeserializer<XnatParserInjector> {

    @Override
    XnatParserInjector deserialize(ObjectCodec objectCodec, JsonNode jsonNode) throws IOException, JsonProcessingException {
        handledType().newInstance() as XnatParserInjector
    }

}
