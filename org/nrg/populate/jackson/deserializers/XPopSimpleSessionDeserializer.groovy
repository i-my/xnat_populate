package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.experiments.SimpleSession
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionExtension
import org.nrg.xnat.jackson.deserializers.CustomDeserializer

class XPopSimpleSessionDeserializer extends CustomDeserializer<SimpleSession> {

    @Override
    SimpleSession deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final SimpleSession simpleSession = new SimpleSession(node.asText())
        simpleSession.setExtension(new DefaultSessionExtension(simpleSession))
        simpleSession
    }

}
