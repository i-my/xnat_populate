package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.enums.SessionSource
import org.nrg.populate.xnat_objects.experiments.SimpleSessionAssessor
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionAssessorExtension
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionExtension
import org.nrg.xnat.pogo.Reconstruction
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.experiments.SessionAssessor
import org.nrg.xnat.pogo.extensions.session_assessor.SessionAssessorExtension
import org.nrg.xnat.pogo.resources.SubjectAssessorResource

class XPopImagingSessionDeserializer extends XPopExperimentDeserializer {

    @Override
    ImagingSession deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        ImagingSession session

        if (node.has('src')) {
            session = SessionSource.values().find { source ->
                source.key == node.get('src').asText()
            }.deserialize(node)
        } else {
            session = new ImagingSession()
            setObject(node, 'pipelines', codec, Map, new DefaultSessionExtension(session).&setPipelines)
        }

        setCommonFields(node, session, codec, SubjectAssessorResource)
        setMapList(node, 'scans', codec, Scan, 'ID', session.&setScans)

        setMapList(node, 'complexAssessors', codec, SessionAssessor, 'label', session.&setAssessors)

        if (fieldNonnull(node, 'assessors')) {
            node.get('assessors').each { assessor ->
                final SessionAssessor sessionAssessor = new SimpleSessionAssessor(assessor.asText())
                final SessionAssessorExtension sessionAssessorExtension = new DefaultSessionAssessorExtension(sessionAssessor)
                session.addAssessor(sessionAssessor)
            }
        }

        setObjectList(node, 'reconstructions', codec, Reconstruction, session.&setReconstructions)

        session
    }

}
