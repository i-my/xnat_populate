package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.extensions.experiments.XPopSubjectAssessorXMLExtension
import org.nrg.xnat.pogo.experiments.NonimagingAssessor
import org.nrg.xnat.pogo.resources.SubjectAssessorResource

class XPopNonimagingAssessorDeserializer extends XPopExperimentDeserializer {

    @Override
    NonimagingAssessor deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final NonimagingAssessor assessor = new NonimagingAssessor()
        final XPopSubjectAssessorXMLExtension extension = new XPopSubjectAssessorXMLExtension(assessor)

        setCommonFields(node, assessor, codec, SubjectAssessorResource)
        assessor
    }
}
