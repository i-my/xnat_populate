package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.jackson.deserializers.CustomDeserializer

class ToStringDeserializer extends CustomDeserializer<String> {

    @Override
    String deserialize(ObjectCodec objectCodec, JsonNode jsonNode) throws IOException, JsonProcessingException {
        jsonNode.isTextual() ? jsonNode.asText() : jsonNode.toString()
    }

}
