package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.extensions.XPopResourceFileExtension
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.extensions.ResourceFileExtension
import org.nrg.xnat.pogo.resources.ResourceFile

class XPopResourceFileDeserializer extends CustomDeserializer<ResourceFile> {

    @Override
    ResourceFile deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final ResourceFile resourceFile = new ResourceFile()
        final ResourceFileExtension extension = new XPopResourceFileExtension(resourceFile)

        setBoolean(node, 'unzip', resourceFile.&setUnzip)
        setStringIfNonnull(node, 'name', resourceFile.&setName)
        setStringIfNonnull(node, 'content', resourceFile.&setContent)

        resourceFile
    }

}
