package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.experiments.Experiment
import org.nrg.xnat.pogo.resources.Resource

abstract class XPopExperimentDeserializer extends CustomDeserializer<Experiment> {

    protected void setCommonFields(JsonNode experimentNode, Experiment experiment, ObjectCodec codec, Class<? extends Resource> resourceClass) {
        setStringIfNonnull(experimentNode, 'label', experiment.&setLabel)
        setStringIfNonnull(experimentNode, 'notes', experiment.&setNotes)
        if (fieldNonnull(experimentNode, 'xsiType')) {
            final DataType dataType = DataType.lookup(experimentNode.get('xsiType').asText())
            experiment.setDataType(dataType)
        }
        if (fieldNonnull(experimentNode, 'fields')) {
            experiment.setSpecificFields(experimentNode.get('fields').fields().collectEntries {
                [(it.key) : it.value.asText()]
            })
        }
        setResources(experimentNode, codec, resourceClass, experiment.&setResources)
        setCustomVariables(experimentNode, codec, experiment)
        XPopShareDeserializer.INSTANCE.setShares(experimentNode, experiment.&setShares)
    }

}
