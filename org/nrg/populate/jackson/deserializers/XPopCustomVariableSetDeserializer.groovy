package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.xnat.jackson.deserializers.CustomDeserializer
import org.nrg.xnat.pogo.DataType
import org.nrg.xnat.pogo.custom_variable.CustomVariable
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet

class XPopCustomVariableSetDeserializer extends CustomDeserializer<CustomVariableSet> {

    @Override
    CustomVariableSet deserialize(ObjectCodec objectCodec, JsonNode node) throws IOException, JsonProcessingException {
        final CustomVariableSet variableSet = new CustomVariableSet()

        if (fieldNonnull(node, 'dataType')) variableSet.setDataType(DataType.lookup(node.get('dataType').asText()))
        setBoolean(node, 'projectSpecific', variableSet.&setIsProjectSpecific)
        setStringIfNonempty(node, 'name', variableSet.&setName)
        setStringIfNonempty(node, 'description', variableSet.&setDescription)
        setObjectList(node, 'variables', objectCodec, CustomVariable, variableSet.&setCustomVariables)

        variableSet
    }

}
