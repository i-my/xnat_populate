package org.nrg.populate.jackson.deserializers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.experiments.SimpleSession
import org.nrg.populate.xnat_objects.extensions.DefaultSubjectExtension
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionExtension
import org.nrg.populate.xnat_objects.extensions.experiments.XPopSubjectAssessorXMLExtension
import org.nrg.xnat.jackson.deserializers.SubjectDeserializer
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.NonimagingAssessor
import org.nrg.xnat.pogo.extensions.subject_assessor.SubjectAssessorExtension
import org.nrg.xnat.pogo.resources.SubjectResource

class XPopSubjectDeserializer extends SubjectDeserializer {

    @Override
    Subject deserialize(ObjectCodec codec, JsonNode node) throws IOException, JsonProcessingException {
        final Subject subject = super.deserialize(codec, node)
        subject.extension(new DefaultSubjectExtension(subject))

        setResources(node, codec, SubjectResource, subject.&setResources)

        XPopShareDeserializer.INSTANCE.setShares(node, subject.&setShares)

        setMapList(node, 'complexSessions', codec, ImagingSession, 'label', subject.&setExperiments)

        if (fieldNonnull(node, 'sessions')) {
            node.get('sessions').each { session ->
                final SimpleSession simpleSession = readObject(session, codec, SimpleSession)
                subject.addExperiment(simpleSession)
            }
        }

        if (fieldNonnull(node, 'assessors')) {
            node.get('assessors').each { assessorNode ->
                final NonimagingAssessor assessor = new NonimagingAssessor().label(assessorNode.asText()) as NonimagingAssessor
                final XPopSubjectAssessorXMLExtension assessorExtension = new XPopSubjectAssessorXMLExtension(assessor)
                subject.addExperiment(assessor)
            }
        }

        subject
    }

}