package org.nrg.populate.jackson.modules

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.populate.injector.XnatParserInjector
import org.nrg.populate.jackson.deserializers.*
import org.nrg.populate.xnat_objects.SiteConfiguration
import org.nrg.populate.xnat_objects.experiments.SimpleSession
import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Reconstruction
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.custom_variable.CustomVariableSet
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.NonimagingAssessor
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.experiments.SessionAssessor
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.reflections.Reflections

class XPopYamlDeserializationModule extends SimpleModule {

    XPopYamlDeserializationModule() {
        super('XPop_Yaml_Deserializers')

        addDeserializer(SiteConfiguration, new XPopSiteConfigDeserializer())
        addDeserializer(AnonScript, new XPopAnonScriptDeserializer())
        addDeserializer(CustomUserGroup, new XPopCustomUserGroupDeserializer())

        addDeserializer(Project, new XPopProjectDeserializer())
        addDeserializer(Subject, new XPopSubjectDeserializer())
        addDeserializer(ImagingSession, new XPopImagingSessionDeserializer())
        addDeserializer(NonimagingAssessor, new XPopNonimagingAssessorDeserializer())
        addDeserializer(SimpleSession, new XPopSimpleSessionDeserializer())
        addDeserializer(Scan, new XPopScanDeserializer())
        addDeserializer(SessionAssessor, new XPopSessionAssessorDeserializer())
        addDeserializer(Reconstruction, new XPopReconstructionDeserializer())

        new Reflections('org.nrg.xnat.pogo.resources').getSubTypesOf(Resource).each { resourceClass ->
            addDeserializer(resourceClass, new XPopResourceDeserializer(resourceClass))
        }
        addDeserializer(ResourceFile, new XPopResourceFileDeserializer())
        addDeserializer(CustomVariableSet, new XPopCustomVariableSetDeserializer())
        addDeserializer(XnatParserInjector, new XPopInjectorDeserializer())
    }

}
