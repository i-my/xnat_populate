package org.nrg.populate.jackson.modules

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.populate.jackson.serializers.*
import org.nrg.xnat.jackson.serializers.InvestigatorSerializer
import org.nrg.xnat.pogo.Investigator
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.Scan
import org.nrg.xnat.pogo.resources.Resource
import org.nrg.xnat.pogo.resources.ResourceFile
import org.nrg.xnat.pogo.users.User

class XPopYamlExportSerializationModule extends SimpleModule {

    XPopYamlExportSerializationModule() {
        super('XPOP_Export_Serializers')

        addSerializer(Project, new XPopExportProjectSerializer())
        addSerializer(Subject, new XPopExportSubjectSerializer())
        //module.addSerializer(ImagingSession, new XPopExportSessionSerializer())
        //module.addSerializer(NonImporterSession, new XPopExportNonImporterSessionSerializer())
        addSerializer(Scan, new XPopExportScanSerializer())
        //module.addSerializer(SessionAssessor, new XPopExportSessionAssessorSerializer())

        addSerializer(Resource, new XPopExportResourceSerializer())
        addSerializer(ResourceFile, new XPopExportResourceFileSerializer())

        addSerializer(User, new XPopExportUserSerializer())
        addSerializer(Investigator, new InvestigatorSerializer())
    }

}
