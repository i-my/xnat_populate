package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import org.nrg.xnat.pogo.experiments.ImagingSession

class XPopExportNonImporterSessionSerializer extends XPopExportSessionSerializer {

    static final XPopExportNonImporterSessionSerializer INSTANCE = new XPopExportNonImporterSessionSerializer()

    @Override
    protected void injectAdditionalFields(JsonGenerator gen, ImagingSession session) {
        gen.writeStringField('src', 'empty')
        gen.writeStringField('xsiType', session.dataType.xsiType)
    }

}
