package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.resources.ResourceFile

class XPopExportResourceFileSerializer extends CustomSerializer<ResourceFile> {

    @SuppressWarnings("GroovyAssignabilityCheck")
    @Override
    void serialize(ResourceFile value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObjectFieldStart(value.name)

        writeBooleanFieldIfTrue(gen, 'unzip', value.unzip)
        writeStringFieldIfNonnull(gen, 'content', value.content)

        gen.writeEndObject()
    }

}
