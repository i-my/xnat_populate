package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.ProjectSerializer
import org.nrg.xnat.pogo.Project

@SuppressWarnings("GroovyAssignabilityCheck")
class XPopExportProjectSerializer extends ProjectSerializer {

    @Override
    void serialize(Project value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject()

        writeStringFieldIfNonnull(gen, 'id', value.id)
        writeStringFieldIfNonnull(gen, 'title', value.title)
        writeStringFieldIfNonnull(gen, 'runningTitle', value.runningTitle)
        writeListFieldIfNonempty(gen, 'aliases', value.aliases)
        writeObjectListAsMap(gen, 'subjects', value.subjects)
        if ((value.owners + value.members + value.collaborators).size() > 0) {
            gen.writeObjectFieldStart('users')
            writeListFieldIfNonempty(gen, 'owners', value.owners)
            writeListFieldIfNonempty(gen, 'members', value.members)
            writeListFieldIfNonempty(gen, 'collaborators', value.collaborators)
            gen.writeEndObject()
        }
        writeObjectFieldIfNonnull(gen, 'pi', value.pi)
        writeListFieldIfNonempty(gen, 'investigators', value.investigators)
        writeXnatResources(gen, value.projectResources)
        // TODO: anon

        writeDescription(value, gen)
        writeKeywords(value, gen)
        writeAccessibility(value, gen)

        gen.writeEndObject()
    }

}
