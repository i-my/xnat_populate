package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.experiments.SessionAssessor

class XPopExportSimpleSessionAssessorSerializer extends CustomSerializer<SessionAssessor> {

    static final XPopExportSimpleSessionAssessorSerializer INSTANCE = new XPopExportSimpleSessionAssessorSerializer()

    @Override
    void serialize(SessionAssessor value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject()
        gen.writeString(value.label)
        gen.writeEndObject()
    }

}
