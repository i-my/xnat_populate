package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.pogo.users.User

class XPopExportUserSerializer extends CustomSerializer<User>{

    @Override
    void serialize(User value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(value.username)
    }

}
