package org.nrg.populate.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.populate.xnat_objects.experiments.SimpleSession
import org.nrg.populate.xnat_objects.extensions.experiments.export.DefaultSessionExportExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.SimpleSessionExtension
import org.nrg.populate.xnat_objects.extensions.experiments.export.SubjectAssessorExportExtension
import org.nrg.xnat.jackson.serializers.SubjectSerializer
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.Experiment
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.NonimagingAssessor
import org.nrg.xnat.pogo.experiments.SubjectAssessor

@SuppressWarnings("GroovyAssignabilityCheck")
class XPopExportSubjectSerializer extends SubjectSerializer {

    @Override
    void serialize(Subject value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObjectFieldStart(value.label)

        writeStandardValues(gen, value)
        writeXnatResources(gen, value.resources)
        writeExperiments(gen, value)
        // TODO: shares

        gen.writeEndObject()
    }

    private void writeExperiments(JsonGenerator gen, Subject subject) {
        final List<SubjectAssessor> imagingSessions, nonimagingAssessors
        (imagingSessions, nonimagingAssessors) = subject.experiments.split { it instanceof ImagingSession }
        final List<ImagingSession> simpleSessions, complexSessions
        (simpleSessions, complexSessions) = imagingSessions.split { it.extension instanceof SimpleSessionExtension }


        writeListFieldIfNonempty(gen, 'sessions', simpleSessions.collect { it.label })
        writeListFieldIfNonempty(gen, 'assessors', nonimagingAssessors.collect { it.label })
        serializeExperimentList(gen, 'complexSessions', complexSessions)
    }

    private serializeExperimentList(JsonGenerator gen, String fieldName, List<SubjectAssessor> experiments) {
        if (!experiments.isEmpty()) {
            gen.writeObjectFieldStart(fieldName)
            experiments.each { experiment ->
                (experiment.extension as SubjectAssessorExportExtension).serializer().serialize(experiment, gen, null)
            }
            gen.writeEndObject()
        }
    }

}
