package org.nrg.populate.enums

enum ActionRequired {
    NONE, DOWNLOAD, UNZIP
}