package org.nrg.populate.enums

import com.fasterxml.jackson.databind.JsonNode
import org.nrg.populate.xnat_objects.experiments.NonImporterSession
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSubjectAssessorExtension
import org.nrg.populate.xnat_objects.extensions.experiments.SmuggledSessionExtension
import org.nrg.xnat.pogo.experiments.ImagingSession

@SuppressWarnings('GroovyResultOfObjectAllocationIgnored')
enum SessionSource {

    EMPTY ('empty') {
        @Override
        ImagingSession deserialize(JsonNode node) {
            final ImagingSession session = new NonImporterSession()
            new DefaultSubjectAssessorExtension(session)
            session
        }
    },
    SMUGGLED ('smuggled') {
        @Override
        ImagingSession deserialize(JsonNode node) {
            final ImagingSession session = new ImagingSession()
            new SmuggledSessionExtension(session)
            session
        }
    }

    String key

    SessionSource(String key) {
        setKey(key)
    }

    abstract ImagingSession deserialize(JsonNode node)

}
