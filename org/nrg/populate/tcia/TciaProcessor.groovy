package org.nrg.populate.tcia

import com.jayway.restassured.RestAssured
import com.jayway.restassured.response.Response
import org.nrg.populate.tcia.filter.TciaDataFilter
import org.nrg.populate.tcia.filter.series.TciaSeriesFilter
import org.nrg.populate.tcia.filter.study.TciaStudyFilter
import org.nrg.populate.util.Globals
import org.nrg.populate.util.PathUtils
import org.nrg.populate.xnat_objects.extensions.DefaultSubjectExtension
import org.nrg.populate.xnat_objects.extensions.XPopProjectSetupExtension
import org.nrg.populate.xnat_objects.extensions.experiments.DefaultSessionExtension
import org.nrg.testing.CollectionUtils
import org.nrg.testing.HttpUtils
import org.nrg.xnat.enums.Gender
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.SubjectAssessor

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class TciaProcessor {

    List<String> readCollectionNames() {
        queryEndpoint('getCollectionValues', ['format' : 'json']).then().assertThat().statusCode(200).and().extract().jsonPath().getList('Collection')
    }

    List<TciaStudy> readStudies(String collection, List<TciaDataFilter> filters) {
        final List<TciaStudy> studies = queryEndpoint('getPatientStudy', ['format' : 'json', 'Collection' : collection]).then().assertThat().statusCode(200).and().extract().response().as(TciaStudy[])
        final List<TciaSeries> series = queryEndpoint('getSeries', ['format' : 'json', 'Collection' : collection]).then().assertThat().statusCode(200).and().extract().response().as(TciaSeries[])
        studies.each { study ->
            study.setSeries(series.findAll { seriesObject ->
                seriesObject.studyInstanceUID == study.studyInstanceUID
            })
        }

        final List<TciaStudyFilter> studyFilters = CollectionUtils.ofType(filters, TciaStudyFilter)
        final List<TciaSeriesFilter> seriesFilters = CollectionUtils.ofType(filters, TciaSeriesFilter)

        studyFilters.each { filter ->
            filter.filter(studies)
        }

        studies.each { study ->
            seriesFilters.each { filter ->
                filter.filter(study.series)
            }
        }

        studies.removeIf { study ->
            study.series.isEmpty()
        }

        studies
    }

    void processProjects() {
        tciaProjects().each { project ->
            final TciaSpec tciaSpec = (project.extension as XPopProjectSetupExtension).tciaSpec
            final Path dataPath = Paths.get(PathUtils.defaultPathForData(project.id))
            dataPath.toFile().mkdirs()

            tciaSpec.collections.each { collection ->
                readStudies(collection, tciaSpec.filters).each { study ->
                    final String xnatSubjectLabel = study.resolveName(tciaSpec.subjectNaming)
                    final Subject xnatSubject = project.subjects.find { it.label == xnatSubjectLabel } ?: readNewSubject(project, xnatSubjectLabel, study)
                    project.addSubject(xnatSubject)

                    final String xnatSessionLabel = study.resolveName(tciaSpec.sessionNaming)
                    final Path sessionZip = dataPath.resolve("${xnatSessionLabel}.zip")
                    if (!sessionZip.toFile().exists()) {
                        final Path sessionDir = Files.createTempDirectory(xnatSessionLabel)
                        study.series.each { series ->
                            println("Pulling series ${series.seriesInstanceUID} from TCIA...")
                            final Path zip = Files.createTempFile(sessionDir, series.seriesInstanceUID, '.zip')
                            final Response response = queryEndpoint('getImage', ['SeriesInstanceUID' : series.seriesInstanceUID])
                            HttpUtils.saveBinaryResponseToFile(response, zip.toFile())
                        }
                        Globals.antBuilder.zip(basedir: sessionDir, destfile: sessionZip)
                    }
                    final ImagingSession xnatSession = new ImagingSession(project, xnatSubject, xnatSessionLabel)
                    xnatSubject.addExperiment(xnatSession.extension(new DefaultSessionExtension(xnatSession)) as SubjectAssessor)
                }
            }
        }
    }

    private Subject readNewSubject(Project project, String label, TciaStudy study) {
        final Subject subject = new Subject(project, label)
        subject.extension(new DefaultSubjectExtension(subject))
        switch (study.patientSex) {
            case ['M', 'm', 'Male', 'Masculino'] :
                subject.gender = Gender.MALE
                break
            case ['F', 'f', 'Female', 'Feminino'] :
                subject.gender = Gender.FEMALE
                break
        }
        subject.src("TCIA (${study.collection})")
    }

    private List<Project> tciaProjects() {
        Globals.projects.findAll { (it.extension as XPopProjectSetupExtension).tciaSpec != null }
    }

    Response queryEndpoint(String endpoint, Map<String, ?> queryParams) {
        RestAssured.given().queryParams(queryParams).get("https://services.cancerimagingarchive.net/services/v3/TCIA/query/${endpoint}")
    }

}
