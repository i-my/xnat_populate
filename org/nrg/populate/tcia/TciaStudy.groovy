package org.nrg.populate.tcia

import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.commons.codec.digest.DigestUtils
import org.nrg.testing.CommonStringUtils

class TciaStudy implements TciaFilterable {

    @JsonProperty('Collection') String collection
    @JsonProperty('PatientID') String patientID
    @JsonProperty('PatientName') String patientName
    @JsonProperty('PatientSex') String patientSex
    @JsonProperty('StudyInstanceUID') String studyInstanceUID
    @JsonProperty('StudyDate') String studyDate
    @JsonProperty('StudyDescription') String studyDescription
    @JsonProperty('PatientAge') String patientAge
    @JsonProperty('SeriesCount') int seriesCount
    List<TciaSeries> series = []

    String resolveName(String templateString) {
        CommonStringUtils.replaceRecursively(templateString, [
                '$patientName' : patientName,
                '$patientID' : patientID,
                '$hashStudyInstanceUID' : new BigInteger(1, DigestUtils.md5(studyInstanceUID)).toString(Character.MAX_RADIX),
                '$studyDate' : studyDate ?: ''
        ])
    }

}
