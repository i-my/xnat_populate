package org.nrg.populate.tcia.filter.series

import org.nrg.populate.tcia.TciaSeries
import org.nrg.populate.tcia.filter.series.TciaSeriesFilter

abstract class SeriesModalityFilter extends TciaSeriesFilter {

    List<String> modalities

    @Override
    void filter(List<TciaSeries> seriesList) {
        seriesList.removeIf { series ->
            isBlacklist() ? modalities.contains(series.modality) : !modalities.contains(series.modality)
        }
    }

    abstract boolean isBlacklist()

}
