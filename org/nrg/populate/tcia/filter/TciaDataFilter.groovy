package org.nrg.populate.tcia.filter

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.nrg.populate.tcia.TciaFilterable
import org.nrg.populate.tcia.filter.series.ModalityBlacklistFilter
import org.nrg.populate.tcia.filter.series.ModalityWhitelistFilter
import org.nrg.populate.tcia.filter.study.MinimumStudyCountFilter
import org.nrg.populate.tcia.filter.study.PatientIDFilter
import org.nrg.populate.tcia.filter.study.PatientSexFilter
import org.nrg.populate.tcia.filter.study.StudiesWithModality

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = 'type'
)
@JsonSubTypes([
    @JsonSubTypes.Type(value = StudiesWithModality, name = 'studiesWithModality'),
    @JsonSubTypes.Type(value = PatientSexFilter, name = 'includedSex'),
    @JsonSubTypes.Type(value = ModalityBlacklistFilter, name = 'modalityBlacklist'),
    @JsonSubTypes.Type(value = ModalityWhitelistFilter, name = 'modalityWhitelist'),
    @JsonSubTypes.Type(value = MinimumStudyCountFilter, name = 'minimumStudyCount'),
    @JsonSubTypes.Type(value = PatientIDFilter, name = 'patientID')
])
abstract class TciaDataFilter<X extends TciaFilterable> {

    abstract void filter(List<X> filterables)

}
