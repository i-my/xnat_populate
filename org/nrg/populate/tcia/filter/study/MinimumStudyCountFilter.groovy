package org.nrg.populate.tcia.filter.study

import org.nrg.populate.tcia.TciaStudy

class MinimumStudyCountFilter extends TciaStudyFilter {

    int numStudies

    @Override
    void filter(List<TciaStudy> studies) {
        studies.removeIf { study ->
            studies.findAll { it.patientID == study.patientID }.size() < numStudies
        }
    }

}
