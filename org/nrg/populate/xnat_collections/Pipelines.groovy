package org.nrg.populate.xnat_collections

import org.nrg.populate.xnat_objects.Pipeline

class Pipelines {

    public static Map<String, Pipeline> pipelines = new HashMap<>()

    static addPipeline(String name, Pipeline pipeline) {
        pipelines[name] = pipeline
    }

    static Pipeline getPipeline(String pipeline) {
        pipelines[pipeline].deepCopy()
    }

    static boolean has(String pipeline) {
        pipelines.containsKey(pipeline)
    }

}
