package org.nrg.populate.xnat_collections

import org.nrg.populate.util.Globals
import org.nrg.xnat.pogo.users.User

class Users {

    public static List<String> users

    static List<String> queryFromXnat() {
        Globals.xnatClient.queryBase().get(Globals.xnatClient.formatXapiUrl('users')).as(List)
    }

    static User constructDefault(String username) {
        new User(username).password(username).firstName('Sample').lastName('User').email('xnatselenium@gmail.com').verified(true).enabled(true)
    }
}
