# XNAT Populate

This is a project that sends data to a 1.7+ [XNAT](http://xnat.org) server, mainly for testing, development, and demo purposes. A list of the data available to populate by default can be found in the [Cheat Sheet](xnat_populate_cheat_sheet.md).

## Prerequisites
XNAT Populate requires a groovy (2.4+) installation with Java 8+. Note that when running it for the first time, it may appear to hang for several minutes as it attempts to resolve all of the dependencies via Grape.

## Parameters/Usage
Run the project by simply invoking the main script
```
#!bash

$ groovy PopulateXnat.groovy <parameters>
```

More information can be found [here](https://wiki.xnat.org/display/XTOOLS/XNAT+Populate).
