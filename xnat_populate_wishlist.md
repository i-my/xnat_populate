The [Quick Summary](xnat_populate_quick_summary.md) document contains a list of all of the SOP Class instances available with XNAT Populate. There are several SOP Classes that have no instances included because I wasn't able to find any open examples on the Internet. If you know where to find any of this type data below, I would be happy to hear it:

| SOP Class UID                                             | SOP Class Name                                                                   |
|-----------------------------------------------------------|----------------------------------------------------------------------------------|
| 1.2.840.10008.5.1.4.1.1.1.1.1                             | Digital X-Ray Image Storage - For Processing                                     |
| 1.2.840.10008.5.1.4.1.1.1.3.1                             | Digital Intra-Oral X-Ray Image Storage - For Processing                          |
| 1.2.840.10008.5.1.4.1.1.4.3                               | Enhanced MR Color Image Storage                                                  |
| 1.2.840.10008.5.1.4.1.1.9.1.3                             | Ambulatory ECG Waveform Storage                                                  |
| 1.2.840.10008.5.1.4.1.1.9.3.1                             | Cardiac Electrophysiology Waveform Storage                                       |
| 1.2.840.10008.5.1.4.1.1.9.4.2                             | General Audio Waveform Storage                                                   |
| 1.2.840.10008.5.1.4.1.1.9.5.1                             | Arterial Pulse Waveform Storage                                                  |
| 1.2.840.10008.5.1.4.1.1.9.6.1                             | Respiratory Waveform Storage                                                     |
| 1.2.840.10008.5.1.4.1.1.11.2                              | Color Softcopy Presentation State Storage                                        |
| 1.2.840.10008.5.1.4.1.1.11.3                              | Pseudo-Color Softcopy Presentation State Storage                                 |
| 1.2.840.10008.5.1.4.1.1.11.5                              | XA/XRF Grayscale Softcopy Presentation State Storage                             |
| 1.2.840.10008.5.1.4.1.1.11.6                              | Grayscale Planar MPR Volumetric Presentation State Storage                       |
| 1.2.840.10008.5.1.4.1.1.11.7                              | Compositing Planar MPR Volumetric Presentation State Storage                     |
| 1.2.840.10008.5.1.4.1.1.11.8                              | Advanced Blending Presentation State Storage                                     |
| 1.2.840.10008.5.1.4.1.1.11.9                              | Volume Rendering Volumetric Presentation State Storage                           |
| 1.2.840.10008.5.1.4.1.1.11.10                             | Segmented Volume Rendering Volumetric Presentation State Storage                 |
| 1.2.840.10008.5.1.4.1.1.11.11                             | Multiple Volume Rendering Volumetric Presentation State Storage                  |
| 1.2.840.10008.5.1.4.1.1.12.2.1                            | Enhanced XRF Image Storage                                                       |
| 1.2.840.10008.5.1.4.1.1.13.1.4                            | Breast Projection X-Ray Image Storage - For Presentation                         |
| 1.2.840.10008.5.1.4.1.1.13.1.5                            | Breast Projection X-Ray Image Storage - For Processing                           |
| 1.2.840.10008.5.1.4.1.1.14.1                              | Intravascular Optical Coherence Tomography Image Storage - For Presentation      |
| 1.2.840.10008.5.1.4.1.1.14.2                              | Intravascular Optical Coherence Tomography Image Storage - For Processing        |
| 1.2.840.10008.5.1.4.1.1.68.1                              | Surface Scan Mesh Storage                                                        |
| 1.2.840.10008.5.1.4.1.1.68.2                              | Surface Scan Point Cloud Storage                                                 |
| 1.2.840.10008.5.1.4.1.1.77.1.1                            | VL Endoscopic Image Storage                                                      |
| 1.2.840.10008.5.1.4.1.1.77.1.2                            | VL Microscopic Image Storage                                                     |
| 1.2.840.10008.5.1.4.1.1.77.1.2.1                          | Video Microscopic Image Storage                                                  |
| 1.2.840.10008.5.1.4.1.1.77.1.3                            | VL Slide-Coordinates Microscopic Image Storage                                   |
| 1.2.840.10008.5.1.4.1.1.77.1.5.2                          | Ophthalmic Photography 16 Bit Image Storage                                      |
| 1.2.840.10008.5.1.4.1.1.77.1.5.5                          | Wide Field Ophthalmic Photography Stereographic Projection Image Storage         |
| 1.2.840.10008.5.1.4.1.1.77.1.5.6                          | Wide Field Ophthalmic Photography 3D Coordinates Image Storage                   |
| 1.2.840.10008.5.1.4.1.1.77.1.5.7                          | Ophthalmic Optical Coherence Tomography En Face Image Storage                    |
| 1.2.840.10008.5.1.4.1.1.77.1.5.8                          | Ophthalmic Optical Coherence Tomography B-scan Volume Analysis Storage           |
| 1.2.840.10008.5.1.4.1.1.78.6                              | Spectacle Prescription Report Storage                                            |
| 1.2.840.10008.5.1.4.1.1.79.1                              | Macular Grid Thickness and Volume Report                                         |
| 1.2.840.10008.5.1.4.1.1.81.1                              | Ophthalmic Thickness Map Storage                                                 |
| 1.2.840.10008.5.1.4.1.1.82.1                              | Corneal Topography Map Storage                                                   |
| 1.2.840.10008.5.1.4.1.1.88.34                             | Comprehensive 3D SR Storage                                                      |
| 1.2.840.10008.5.1.4.1.1.88.35                             | Extensible SR Storage                                                            |
| 1.2.840.10008.5.1.4.1.1.88.40                             | Procedure Log Storage                                                            |
| 1.2.840.10008.5.1.4.1.1.88.65                             | Chest CAD SR Storage                                                             |
| 1.2.840.10008.5.1.4.1.1.88.68                             | Radiopharmaceutical Radiation Dose SR Storage                                    |
| 1.2.840.10008.5.1.4.1.1.88.69                             | Colon CAD SR Storage                                                             |
| 1.2.840.10008.5.1.4.1.1.88.70                             | Implantation Plan SR Document Storage                                            |
| 1.2.840.10008.5.1.4.1.1.88.71                             | Acquisition Context SR Storage                                                   |
| 1.2.840.10008.5.1.4.1.1.88.72                             | Simplified Adult Echo SR Storage                                                 |
| 1.2.840.10008.5.1.4.1.1.88.73                             | Patient Radiation Dose SR Storage                                                |
| 1.2.840.10008.5.1.4.1.1.88.74                             | Planned Imaging Agent Administration SR Storage                                  |
| 1.2.840.10008.5.1.4.1.1.88.75                             | Performed Imaging Agent Administration SR Storage                                |
| 1.2.840.10008.5.1.4.1.1.104.3                             | Encapsulated STL Storage                                                         |
| 1.2.840.10008.5.1.4.1.1.130                               | Enhanced PET Image Storage                                                       |
| 1.2.840.10008.5.1.4.1.1.200.2                             | CT Performed Procedure Protocol Storage                                          |
| 1.2.840.10008.5.1.4.1.1.481.4                             | RT Beams Treatment Record Storage                                                |
| 1.2.840.10008.5.1.4.1.1.481.6                             | RT Brachy Treatment Record Storage                                               |
| 1.2.840.10008.5.1.4.1.1.481.10                            | RT Physician Intent Storage                                                      |
| 1.2.840.10008.5.1.4.1.1.481.11                            | RT Segment Annotation Storage                                                    |
| 1.2.840.10008.5.1.4.34.7                                  | RT Beams Delivery Instruction Storage                                            |
| 1.2.840.10008.5.1.4.34.10                                 | RT Brachy Application Setup Delivery Instruction Storage                         |
